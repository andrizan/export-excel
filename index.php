<?php
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$servername = "localhost";
$username = "admin";
$password = "admin";

try {
	$conn = new PDO("mysql:host=$servername;dbname=db_indo", $username, $password);
	// set the PDO error mode to exception
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$data = $conn->query('SELECT * FROM country');
	// var_dump($data->fetchAll(PDO::FETCH_OBJ));

	//create new excel (no template)
	// $spreadsheet = new Spreadsheet();

	$inputFileName = './template/country.xlsx';
	/** Load $inputFileName to a Spreadsheet Object  **/
	$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);
	$sheet = $spreadsheet->getActiveSheet();
	$no = 4;

	$styleArray = [	
		'borders' => [
			'allBorders' => [
				'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
			],
		],
	];
	/** You can achieve any border effect by using just the 5 basic borders and operating on a single cell at a time:

	- left
	- right
	- top
	- bottom
	- diagonal

	Additional shortcut borders come in handy like in the example above. These are the shortcut borders available:

	- allBorders
	- outline
	- inside
	- vertical
	- horizontal **/
	foreach ($data->fetchAll(PDO::FETCH_OBJ) as $val) { $no++;
		$sheet
			->setCellValue('A'.$no, $val->iso)
			->setCellValue('B'.$no, $val->name)
			->setCellValue('C'.$no, $val->nicename)
			->setCellValue('D'.$no, $val->iso3)
			->setCellValue('E'.$no, $val->numcode)
			->setCellValue('F'.$no, $val->phonecode);
		$spreadsheet->getActiveSheet()->getStyle('A'.$no.':F'.$no)->applyFromArray($styleArray);
	}
	$writer = new Xlsx($spreadsheet);
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment; filename="country.xlsx"');
	$writer->save('php://output');
	$conn = null;
}catch(PDOException $e){
	echo "Connection failed: " . $e->getMessage();
}
